package net.therpservices.simpleWebserver.util;

import java.io.*;

/**
 * Created by rifatul on 5/7/14.
 */
public class HttpRequestHandler {
    private int length;
    private String[] requestHeader;
    private StringBuilder httpReqBuffer;
    private StringBuilder body;
    private BufferedReader inputStream = null;
    private FileInputStream fileInputStream = null;
    private OutputStream outputStream = null;


    public HttpRequestHandler(InputStream socketInputStream, OutputStream socketOutputStream) throws IOException {
        inputStream = new BufferedReader(new InputStreamReader(socketInputStream));
        outputStream = socketOutputStream;
        httpReqBuffer = new StringBuilder();
        body = new StringBuilder();
        requestHeader = new String[0];
        isRequestValid();
    }

    private void isRequestValid() throws IOException {
        String requestLine = inputStream.readLine();
        if (requestLine.startsWith("GET") || requestLine.startsWith("POST")) {
            requestHeader = requestLine.split(" ");
            httpReqBuffer.append(requestLine).append("\n");
            parseRequestFromSocket();

        } else {
            writeToSocketStream(StatusCode.BAD_REQUEST, StatusCode.getBadRequestErrMsg());
        }
    }

    private void parseRequestFromSocket() throws IOException {
        String httpRequest;
        while ((httpRequest = inputStream.readLine()) != null) {

            if (httpRequest.equals("")) {
                break;
            }
            if (httpRequest.startsWith("Content-Length: ")) {
                int index = httpRequest.indexOf(':') + 1;
                String len = httpRequest.substring(index).trim();
                length = Integer.parseInt(len);
            }
            httpReqBuffer.append(httpRequest).append("\n");
        }
    }

    public void writeToSocketStream(String status, byte[] message) throws IOException {
        outputStream.flush();
        addResponseHeader(status);
        outputStream.write(message);
    }

    public void writeFileToOutputStream(String fileName, String status) throws IOException {
        File file = new File(fileName);
        byte[] bFile = new byte[(int) file.length()];
        fileInputStream = new FileInputStream(file);
        fileInputStream.read(bFile);
        fileInputStream.close();

        outputStream.flush();
        addResponseHeader(status);
        outputStream.write(bFile, 0, bFile.length);
    }

    public String getRequestHeader() throws IOException {
        return httpReqBuffer.toString();

    }

    public String getRequestType() throws IOException {
        return requestHeader[0];
    }

    public String getHttpRequest() throws IOException {
        String resourcePath = requestHeader[1];
        resourcePath = resourcePath.replaceFirst("/", "");
        if (resourcePath.equals("web/")||resourcePath.equals("web")) {
            resourcePath = "web/index.html";
        }
        if(resourcePath.equals("")){
            writeToSocketStream(StatusCode.ACCESS_DENIED,StatusCode.getAccessDeniedErrMsg());
        }
        return resourcePath;
    }

    public void closeStream() throws IOException {
        if (fileInputStream != null) {
            fileInputStream.close();
            inputStream.close();
            outputStream.close();
        }
    }

    public void addResponseHeader(String status) throws IOException {
        outputStream.write(status.getBytes());
    }

    public String getPostData() throws IOException {
        if (length > 0) {
            int read;
            while ((read = inputStream.read()) != -1) {
                body.append((char) read);
                if (body.length() == length)
                    break;
            }
        }
        return body.toString();
    }
}
