package net.therpservices.simpleWebserver.client;

import net.therpservices.simpleWebserver.util.HttpRequestHandler;
import net.therpservices.simpleWebserver.util.StatusCode;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: rifatul.islam
 * Date: 5/6/14
 * Time: 10:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class ClientRequestProcessor implements Runnable {
    HttpRequestHandler httpRequestHandler = null;
    private Socket clientSocket;

    public ClientRequestProcessor(Socket clientSocket) {
        this.clientSocket = clientSocket;

    }

    @Override
    public void run() {
        try {
            httpRequestHandler = new HttpRequestHandler(clientSocket.getInputStream(),
                    clientSocket.getOutputStream());

            System.out.println(httpRequestHandler.getRequestHeader());
            String httpRequest = httpRequestHandler.getHttpRequest();
            String requestType = httpRequestHandler.getRequestType();

            if (requestType.equals("POST")) {
                System.out.println(httpRequestHandler.getPostData());
            }

            System.out.println("httpRequest = " + httpRequest);
            if (!httpRequest.isEmpty())
                httpRequestHandler.writeFileToOutputStream(httpRequest, StatusCode.OK);

        } catch (FileNotFoundException ex) {
            try {
                httpRequestHandler.writeToSocketStream(StatusCode.FILE_NOT_FOUND, StatusCode.getFileNotFoundErrMsg());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpRequestHandler.closeStream();
                clientSocket.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
